FROM python:3.6

COPY requirements.txt /tmp/requirements.txt
RUN  pip install -r   /tmp/requirements.txt

COPY app /app/

RUN useradd flask && chown -R flask /app
USER flask

WORKDIR /app
EXPOSE 8000
CMD ["uvicorn", "--host", "0.0.0.0", "--port", "8000", "sendsms:app"]
